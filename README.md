## ?

A system for parsing incomming messages and syncinc multiple product stocks

##  approach
* The initial communication will be put through a queue, a PriorityQueue to be exact so the events can be sorted by timestamps
* Event putting and queue reading should not be syncronized - both will be run in Threads
* Products need to communicate somehow between themselves and other entities - I decided on a standard Publisher/Subscriber pattern not to use queues again ;)
* Products parameters are classes with **descriptors**

* Products have to be created when an appropriate event is parsed - ProductCreator helper will be responsible for that
* When last event from the file is parsed the summary line has to be written, it will also use the PubSub mechanism

## Usage

```
app.py [-h] [-d <delay>] -i <inputfile> -o <outputfile>
```

h - prints help  
d <delay> - delay between event sending in milliseconds (+/- 5ms)  
i <inputfile> - path to the input file  
o <outputfile> - path to the output file
