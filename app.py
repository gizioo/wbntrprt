#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, getopt
from time import sleep
from random import randint
from model import ProductCreator, Product
from myqueue import *
from pubsub import *
from utils import *


def main(argv):
    input_file = ''
    output_file = 'outfile.txt'
    delay = 0

    try:
      opts, args = getopt.getopt(argv,"hd:i:o:",["delay=","ifile=","ofile="])

    except getopt.GetoptError:
      print('app.py [-h {print help}] [-d <delay>] -i <inputfile> -o <outputfile>')
      sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('app.py [-h {print help}] [-d <delay>] -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg
        elif opt in ("-d", "--delay"):
            delay = arg
    if not input_file:
        print('app.py [-h {print help}] [-d <delay>] -i <inputfile> -o <outputfile>')
        sys.exit()

    #init the queue
    my_queue = MyPriorityQueue()

    #init the event producer
    producer = Producer(input_file, my_queue, delay)

    #initialize the publisher
    publisher = Publisher()

    #initialize the queue consumer
    consumer = Consumer(my_queue, publisher)


    #initialize the printer for summary
    printer = Printer(publisher, Product.instances)

    #initialize the file writer
    file_writer = FileWriter(output_file)

    #subscribe printer and file writer to catch appropriate events
    publisher.subscribe(printer, 'Print', '*')
    publisher.subscribe(file_writer, 'EndProduct', '*')
    publisher.subscribe(file_writer, 'UpdateProduct', '*')
    publisher.subscribe(file_writer, 'StockSummary', '*')

    #initialize the product creator
    product_creator = ProductCreator(publisher)

    #start producing an consuming
    producer.start()
    consumer.start()

if __name__ == '__main__':
    main(sys.argv[1:])
