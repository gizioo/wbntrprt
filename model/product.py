#!/usr/bin/env python
# -*- coding: utf-8 -*-

from weakref import WeakKeyDictionary


class ProductID(object):
    """ProductID class. Totaly unnecessary but made for Mateusz Czubak due to @DESCRIPTORS
    """

    def __init__(self):
        self.default = None
        self.product_ids = WeakKeyDictionary()

    def __get__(self, instance, owner):
        return self.product_ids.get(instance, self.default)

    def __set__(self, instance, product_id):
        if product_id and product_id <= 0:
            raise ValueError("ID must be higher than 0.")
        self.product_ids[instance] = product_id

    def __delete__(self, instance):
        del self.product_ids[instance]


class Stock(object):
    """Stock class. Totaly unnecessary but made for Mateusz Czubak due to @DESCRIPTORS
    """

    def __init__(self):
        self.default = None
        self.stock = WeakKeyDictionary()

    def __get__(self, instance, owner):
        return self.stock.get(instance, self.default)

    def __set__(self, instance, stock):
        if stock < 0:
            raise ValueError("Stock cannot be a negative value")
        self.stock[instance] = stock

    def __delete__(self, instance):
        del self.stock[instance]


class Product(object):
    """Main Product class. Parsed the events from the input file, updates itself if necessary
    and notifies other Products in the tree about the changes taken.
    """

    instances = []
    product_id = ProductID()
    stock = Stock()
    parent_id = ProductID()
    ended = False

    def __init__(self, product_id, stock, parent_id, publisher):
        self.product_id = product_id
        self.stock = stock
        self.parent_id = parent_id
        self.ended = False
        self.publisher = publisher
        Product.instances.append(self)

    def emit_event(self, data, event_type, product_id):
        self.publisher.publish(data, event_type, product_id)

    def update(self, data):
        """Method called when event is published and the product subscribes the given event
        """

        #parse basic events to set proeducts state
        if data.get('type', None) == 'ProductUpdated':
            new_stock = data.get('stock', None)
            if new_stock is not None:
                self.stock = data.get('stock')

                if new_stock == 0:
                    if self.parent_id:
                        self.emit_event({'type': 'EndProduct',
                                        'id': self.parent_id},
                                        'EndProduct',
                                        self.parent_id)
                        for product_id in Product.get_children(self.parent_id, exclude=self.product_id):
                            self.emit_event({'type': 'EndProduct',
                                            'id': product_id},
                                            'EndProduct', product_id)
                    else:
                        for product_id in Product.get_children(self.product_id):
                            self.emit_event({'type': 'EndProduct', 'id': product_id},
                                            'EndProduct', product_id)
                else:
                    #yeah yeah long lines, pep8 blah blah, I know, but c'mon... 
                    if self.parent_id:
                        self.emit_event({'type': 'UpdateProduct', 'stock': new_stock, 'id': self.parent_id},
                                        'UpdateProduct', self.parent_id)
                        for product_id in Product.get_children(self.parent_id, exclude=self.product_id):
                            self.emit_event({'type': 'UpdateProduct', 'stock': new_stock, 'id': product_id},
                                            'UpdateProduct', product_id)
                    else:
                        for product_id in Product.get_children(self.product_id):
                            self.emit_event({'type': 'UpdateProduct', 'stock': new_stock, 'id': product_id},
                                            'UpdateProduct', product_id)

        elif data.get('type', None) == 'ProductEnded':
            
            self.ended = True
            for product_id in Product.get_children(self.product_id):
                self.emit_event({'type': 'EndProduct'}, 'EndProduct', product_id)

        #parse actual update and end events
        elif data.get('type', None) == 'EndProduct':
            self.ended = True

        elif data.get('type', None) == 'UpdateProduct':
            self.stock = data.get('stock')

    @staticmethod
    def get_children(product_id, exclude=None):
        children = []
        for i in Product.instances:
            if i.parent_id == product_id and i.product_id != exclude:
                children.append(i.product_id)
        return children
