#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .product import Product


class ProductCreator(object):

    """Product creator class. When subscribed to an event creates products
    and subscribes them to appropriate events.

    input arguments:
    publisher -- PubSub object
    """
    def __init__(self, publisher):
        self.publisher = publisher
        self.publisher.subscribe(self, 'ProductCreated', '*')

    def update(self, data):
        product_id = data.get('id')
        stock = data.get('stock')
        parent_id = data.get('parent_id', None)

        product = Product(product_id, stock, parent_id, self.publisher)
        self.publisher.subscribe(product, 'ProductUpdated', product_id)
        self.publisher.subscribe(product, 'ProductEnded', product_id)
        self.publisher.subscribe(product, 'UpdateProduct', product_id)
        self.publisher.subscribe(product, 'EndProduct', product_id)
