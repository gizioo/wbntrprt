#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import json
import time
from queue import PriorityQueue
from threading import Thread
from random import randint


class MyPriorityQueue(PriorityQueue):
    """Priority Queue implementation.
    PriorityQueue gives a chance of properly sorting delayed events based on timestamp
    if the other events are still processed.
    """

    def __init__(self):
        PriorityQueue.__init__(self)

    def put(self, item, priority):
        PriorityQueue.put(self, (priority, item))

    def get(self, *args, **kwargs):
        _, item = PriorityQueue.get(self, *args, **kwargs)
        return item


class Producer():
    """Event producer for the queue. Delay is used to simulate a more real-life situatuon when
    events come with random time"""

    def __init__(self, file_path, queue, delay=None):
        self.file_path = file_path
        self.queue = queue
        self.delay = int(delay) if delay else None

    def produce(self):
        """Reads input file line by line, parses the line to python object and puts an item into the queue
        """

        with open(self.file_path, "r") as f:
            for line in f.readlines():
                task = self.parse_line(line)
                if self.delay:
                    time.sleep((self.delay+randint(-5,5))/1000)
                self.queue.put(task, task.get('timestamp'))
            timestamp = int(time.time())
            self.queue.put({"type": "Print", "timestamp": timestamp}, timestamp)

    def start(self):
        """Creates a new Thread for the producer to work not in sync with consumer
        """

        Thread(target=self.produce).start()

    @staticmethod
    def parse_line(line):
        return json.loads(re.match("({.*}).*",line)
                            .groups()[0]
                            .replace('\'','\"')
                            .replace('None','null'))

class Consumer():
    """Queue consumer. Takes a task from the queueand publishes to the PubSub
    """

    def __init__(self, queue, publisher):
        self.queue = queue
        self.publisher = publisher
        self.last_timestamp = 0

    def consume(self):
        while True:
            task = self.queue.get()

            #drop late events
            if task.get('timestamp') < self.last_timestamp:
                continue
            else:
                self.last_timestamp = task.get('timestamp')

                if task.get('type') == 'Print':
                    self.publisher.publish(task, task.get('type'), '*')
                    break

                self.publisher.publish(task, task.get('type'), task.get('id'))

    def start(self):
        """Start a new thread for the consumer to work not in sync with producer
        """

        Thread(target=self.consume).start()
