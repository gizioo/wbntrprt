#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Publisher(object):
    """PubSub for managing event driven communication between Products and other misc objects.
    Calls objects update method with event data as the argument.
    """

    def __init__(self):
        self.subscriptions = {}

    #subscribe to events based on topic and key (id in most cases)
    def subscribe(self, subscriber, topic, key='*'):
        if topic not in self.subscriptions:
            self.subscriptions[topic] = {}
        if key not in self.subscriptions[topic]:
            self.subscriptions[topic][key] = []

        self.subscriptions[topic][key].append(subscriber)

    #publish events
    def publish(self, data, topic, key='*'):
        if topic in self.subscriptions and self.subscriptions.get(topic).get(key, None):
            for subscriber in self.subscriptions[topic][key]:
                subscriber.update(data)
        elif topic not in self.subscriptions:
            pass

        #publish topic to global listeners, eg. file writer
        if key != '*':
            self.publish(data, topic, '*')
