#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Printer(object):
    """Printer util that will publish a summary event for the writer to catch
    """

    def __init__(self, publisher, prod_instances):
        self.publisher = publisher
        self.prod_instances = prod_instances

    def update(self, _):
        self.publisher.publish({'type': 'StockSummary',
                                'stocks': {
                                    str(i.product_id): i.stock for i in self.prod_instances}
                                },
                                "StockSummary", "*")


class FileWriter(object):
    """Writes lines to file
    """

    def __init__(self, file_path):
        self.f = open(file_path,"w")

    def __del__(self):
        self.f.close()

    def update(self, data=None):
        self.f.write("%s\n"%str(data))
